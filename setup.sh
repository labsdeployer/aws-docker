#!/bin/sh
set -e

sudo curl -fsSL get.docker.com -o get-docker.sh
sudo sh get-docker.sh

sudo mv docker.config /etc/init/docker.conf
sudo service docker restart

# sudo mv sshd_config /etc/ssh/sshd_config
# sudo service ssh restart
